const env = require('node-env-file');
env(__dirname + '/.env');

const API_PORT = process.env.API_PORT



module.exports = { 
    API_PORT 
}